#giving the path of file to read
path = "C:/Users/ssathi/Desktop/fileoperations.txt"
#creating a function to read file
def read_file1(path):
	#opening the file in read mode
  f = open(path, "rt")
	#if the file exists then it reads the file
  if f:
    print(f.read())
		#if the file does not exist it displays a message as file does not exist
  else:
    print("File Does not exist")

#calling the function to read the file
read_file1(path)
